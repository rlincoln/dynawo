 ====== INIT VARIABLES VALUES ======
generator_lambdaDPu                               : y =        1.05459 yp =    -0.00019186
generator_lambdaQ1Pu                              : y =     -0.0731468 yp =   -6.34411e-05
generator_lambdaQ2Pu                              : y =     -0.0731468 yp =   -0.000305577
generator_lambdafPu                               : y =        1.15438 yp =   -9.58821e-05
generator_omegaPu_value                           : y =              1 yp =   -6.88353e-06
generator_omegaRefPu_value                        : y =              1 yp =              0
generator_terminal_V_im                           : y =     -0.0907515 yp =              0
generator_terminal_V_re                           : y =         1.0411 yp =              0
generator_theta                                   : y =    -0.00748079 yp =   -1.26607e-09
Pm_setPoint_value                                 : y =      0.0396926 yp =              0
URef_setPoint_value                               : y =        1.14484 yp =              0
generator_IRotorPu_value                          : y =        1.14824 yp =              0
generator_IStatorPu_value                         : y =       0.566949 yp =              0
generator_PGen                                    : y =        40.0971 yp =              0
generator_PGenPu                                  : y =       0.400971 yp =              0
generator_PePu                                    : y =      0.0358101 yp =              0
generator_PmPu_value                              : y =      0.0396926 yp =              0
generator_QGen                                    : y =        43.6197 yp =              0
generator_QGenPu                                  : y =       0.436197 yp =              0
generator_QStatorPu_value                         : y =        0.46834 yp =              0
generator_UPu                                     : y =        1.04505 yp =              0
generator_UStatorPu_value                         : y =        1.08747 yp =              0
generator_cePu                                    : y =      0.0358101 yp =              0
generator_cmPu                                    : y =      0.0396926 yp =              0
generator_efdPu_value                             : y =        1.14738 yp =              0
generator_iDPu                                    : y =    3.36884e-05 yp =              0
generator_iQ1Pu                                   : y =     2.4506e-05 yp =              0
generator_iQ2Pu                                   : y =    5.07958e-05 yp =              0
generator_iStatorPu_im                            : y =       0.449135 yp =              0
generator_iStatorPu_re                            : y =       -0.34599 yp =              0
generator_idPu                                    : y =     -0.0398691 yp =              0
generator_ifPu                                    : y =       0.488403 yp =              0
generator_iqPu                                    : y =     -0.0311911 yp =              0
generator_lambdadPu                               : y =        1.04186 yp =              0
generator_lambdaqPu                               : y =     -0.0831031 yp =              0
generator_sStatorPu_im                            : y =       -0.46834 yp =              0
generator_sStatorPu_re                            : y =      -0.400971 yp =              0
generator_terminal_i_im                           : y =       0.449135 yp =              0
generator_terminal_i_re                           : y =       -0.34599 yp =              0
generator_thetaInternal_value                     : y =      0.0794681 yp =              0
generator_uStatorPu_im                            : y =     -0.0561526 yp =              0
generator_uStatorPu_re                            : y =        1.08602 yp =              0
generator_udPu                                    : y =      0.0829608 yp =              0
generator_ufPu                                    : y =    0.000411323 yp =              0
generator_uqPu                                    : y =        1.04175 yp =              0
governor_PmPu_value                               : y =      0.0396926 yp =              0
governor_PmRawPu_u1                               : y =      0.0396926 yp =              0
governor_PmRawPu_u2                               : y =    2.00489e-11 yp =              0
governor_PmRawPu_y                                : y =      0.0396926 yp =              0
governor_PmRefPu_value                            : y =      0.0396926 yp =              0
governor_feedback_u1                              : y =              1 yp =              0
governor_feedback_u2                              : y =              1 yp =              0
governor_feedback_y                               : y =    4.00978e-12 yp =              0
governor_gain_u                                   : y =    4.00978e-12 yp =              0
governor_gain_y                                   : y =    2.00489e-11 yp =              0
governor_limiter_u                                : y =      0.0396926 yp =              0
governor_limiter_y                                : y =      0.0396926 yp =              0
governor_omegaPu_value                            : y =              1 yp =              0
governor_omegaRefPu_y                             : y =              1 yp =              0
voltageRegulator_EfdPu                            : y =        1.14738 yp =              0
voltageRegulator_EfdPuPin_value                   : y =        1.14738 yp =              0
voltageRegulator_UcEfdPu                          : y =        1.14484 yp =              0
voltageRegulator_UsPu                             : y =        1.08747 yp =              0
voltageRegulator_feedback_u1                      : y =        1.14484 yp =              0
voltageRegulator_feedback_u2                      : y =        1.08747 yp =              0
voltageRegulator_feedback_y                       : y =      0.0573692 yp =              0
voltageRegulator_gain_u                           : y =      0.0573692 yp =              0
voltageRegulator_gain_y                           : y =        1.14738 yp =              0
voltageRegulator_limiterWithLag_u                 : y =        1.14738 yp =              0
voltageRegulator_limiterWithLag_y                 : y =        1.14738 yp =              0
 ====== INIT DISCRETE VARIABLES VALUES ======
voltageRegulator_limiterWithLag_tUMaxReached      : z =          1e+60
voltageRegulator_limiterWithLag_tUMinReached      : z =          1e+60
generator_running_value                           : z =              1
generator_switchOffSignal1_value                  : z =             -1
generator_switchOffSignal2_value                  : z =             -1
generator_switchOffSignal3_value                  : z =             -1
voltageRegulator_limitationDown_value             : z =             -1
voltageRegulator_limitationUp_value               : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMax  : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMin  : z =             -1
generator_state                                   : z =              2
governor_state                                    : z =              1
 ====== PARAMETERS VALUES ======
Pm_Value0                                          =      0.0396926
URef_Value0                                        =        1.14484
generator_Ce0Pu                                    =      0.0357234
generator_Cm0Pu                                    =      0.0396926
generator_DPu                                      =              0
generator_Efd0Pu                                   =        1.14818
generator_H                                        =            6.3
generator_IRotor0Pu                                =        1.14818
generator_IStator0Pu                               =       0.565888
generator_Id0Pu                                    =     -0.0398099
generator_If0Pu                                    =        0.48838
generator_Iq0Pu                                    =     -0.0311131
generator_Kuf                                      =    0.000358487
generator_LDPPu                                    =       0.142318
generator_LQ1PPu                                   =       0.261099
generator_LQ2PPu                                   =       0.125948
generator_LambdaD0Pu                               =        1.05459
generator_LambdaQ10Pu                              =     -0.0731468
generator_LambdaQ20Pu                              =     -0.0731468
generator_Lambdad0Pu                               =        1.04189
generator_Lambdaf0Pu                               =        1.15438
generator_Lambdaq0Pu                               =     -0.0830719
generator_LdPPu                                    =          0.219
generator_LfPPu                                    =        0.20434
generator_LqPPu                                    =          0.219
generator_MdPPu                                    =          2.351
generator_MqPPu                                    =          2.351
generator_MrcPPu                                   =              0
generator_P0Pu                                     =           -0.4
generator_PGen0Pu                                  =            0.4
generator_PNom                                     =           1008
generator_Pm0Pu                                    =      0.0396926
generator_Q0Pu                                     =        -0.4356
generator_QGen0Pu                                  =         0.4356
generator_QStator0Pu                               =       0.467623
generator_RDPPu                                    =      0.0181282
generator_RQ1PPu                                   =      0.0082404
generator_RQ2PPu                                   =      0.0191489
generator_RTfPu                                    =              0
generator_RTfoPu                                   =              0
generator_RaPPu                                    =        0.00357
generator_RfPPu                                    =    0.000842804
generator_SNom                                     =           1120
generator_SnTfo                                    =           1120
generator_Theta0                                   =    -0.00748079
generator_ThetaInternal0                           =    -0.00748079
generator_U0Pu                                     =        1.04507
generator_UBaseHV                                  =             69
generator_UBaseLV                                  =             24
generator_UNom                                     =             24
generator_UNomHV                                   =             69
generator_UNomLV                                   =             24
generator_UPhase0                                  =     -0.0869174
generator_UStator0Pu                               =        1.08743
generator_Ud0Pu                                    =      0.0829297
generator_Uf0Pu                                    =    0.000411608
generator_Uq0Pu                                    =        1.04178
generator_XTfPu                                    =            0.1
generator_XTfoPu                                   =            0.1
generator_i0Pu_im                                  =       0.448465
generator_i0Pu_re                                  =      -0.345121
generator_iStator0Pu_im                            =       0.448465
generator_iStator0Pu_re                            =      -0.345121
generator_rTfoPu                                   =              1
generator_s0Pu_im                                  =        -0.4356
generator_s0Pu_re                                  =           -0.4
generator_sStator0Pu_im                            =      -0.467623
generator_sStator0Pu_re                            =           -0.4
generator_u0Pu_im                                  =     -0.0907207
generator_u0Pu_re                                  =        1.04113
generator_uStator0Pu_im                            =     -0.0562085
generator_uStator0Pu_re                            =        1.08597
governor_KGover                                    =              5
governor_PMax                                      =           1008
governor_PMaxPu                                    =              1
governor_PMin                                      =              0
governor_PMinPu                                    =              0
governor_PNom                                      =           1008
governor_Pm0Pu                                     =      0.0396926
governor_PmRawPu_k1                                =              1
governor_PmRawPu_k2                                =              1
governor_gain_k                                    =              5
governor_limiter_uMax                              =              1
governor_limiter_uMin                              =              0
governor_omegaRefPu_k                              =              1
voltageRegulator_Efd0Pu                            =        1.14818
voltageRegulator_Efd0PuLF                          =        1.14818
voltageRegulator_EfdMaxPu                          =              5
voltageRegulator_EfdMinPu                          =             -5
voltageRegulator_Gain                              =             20
voltageRegulator_LagEfdMax                         =              0
voltageRegulator_LagEfdMin                         =              0
voltageRegulator_UcEfd0Pu                          =        1.14484
voltageRegulator_Us0Pu                             =        1.08743
voltageRegulator_gain_k                            =             20
voltageRegulator_limiterWithLag_LagMax             =              0
voltageRegulator_limiterWithLag_LagMin             =              0
voltageRegulator_limiterWithLag_UMax               =              5
voltageRegulator_limiterWithLag_UMin               =             -5
voltageRegulator_limiterWithLag_tUMaxReached0      =          1e+60
voltageRegulator_limiterWithLag_tUMinReached0      =          1e+60
voltageRegulator_limiterWithLag_u0                 =        1.14818
voltageRegulator_limiterWithLag_y0                 =        1.14818
voltageRegulator_tEfdMaxReached0                   =          1e+60
voltageRegulator_tEfdMinReached0                   =          1e+60
governor_limiter_limitsAtInit                      =              1
governor_limiter_strict                            =             -1
generator_ExcitationPu                             =              1
generator_NbSwitchOffSignals                       =              3
generator_State0                                   =              2
