 ====== INIT VARIABLES VALUES ======
generator_lambdaDPu                               : y =       0.980253 yp =   -4.31753e-05
generator_lambdaQ1Pu                              : y =   -5.36918e-09 yp =   -3.27413e-05
generator_lambdaQ2Pu                              : y =   -5.36918e-09 yp =   -0.000153437
generator_lambdafPu                               : y =        1.06561 yp =   -2.27553e-05
generator_omegaPu_value                           : y =              1 yp =   -3.58029e-06
generator_omegaRefPu_value                        : y =              1 yp =              0
generator_terminal_V_im                           : y =      -0.147758 yp =              0
generator_terminal_V_re                           : y =       0.968786 yp =              0
generator_theta                                   : y =      -0.151323 yp =   -5.71812e-07
Pm_setPoint_value                                 : y =    2.49216e-09 yp =              0
URef_setPoint_value                               : y =        1.02999 yp =              0
generator_IRotorPu_value                          : y =       0.982141 yp =              0
generator_IStatorPu_value                         : y =     0.00922023 yp =              0
generator_PGen                                    : y =      0.0867176 yp =              0
generator_PGenPu                                  : y =    0.000867176 yp =              0
generator_PePu                                    : y =    7.74288e-05 yp =              0
generator_PmPu_value                              : y =    1.18798e-08 yp =              0
generator_QGen                                    : y =       0.899402 yp =              0
generator_QGenPu                                  : y =     0.00899402 yp =              0
generator_QStatorPu_value                         : y =     0.00900252 yp =              0
generator_UPu                                     : y =       0.979989 yp =              0
generator_UStatorPu_value                         : y =       0.980907 yp =              0
generator_cePu                                    : y =    7.74288e-05 yp =              0
generator_cmPu                                    : y =    1.18798e-08 yp =              0
generator_efdPu_value                             : y =       0.981736 yp =              0
generator_iDPu                                    : y =    1.52172e-05 yp =              0
generator_iQ1Pu                                   : y =    2.49601e-05 yp =              0
generator_iQ2Pu                                   : y =    5.12416e-05 yp =              0
generator_iStatorPu_im                            : y =     0.00920617 yp =              0
generator_iStatorPu_re                            : y =    0.000508997 yp =              0
generator_idPu                                    : y =   -0.000819437 yp =              0
generator_ifPu                                    : y =       0.417755 yp =              0
generator_iqPu                                    : y =   -7.89833e-05 yp =              0
generator_lambdadPu                               : y =       0.979989 yp =              0
generator_lambdaqPu                               : y =   -3.17353e-05 yp =              0
generator_sStatorPu_im                            : y =    -0.00900252 yp =              0
generator_sStatorPu_re                            : y =   -0.000867176 yp =              0
generator_terminal_i_im                           : y =     0.00920617 yp =              0
generator_terminal_i_re                           : y =    0.000508997 yp =              0
generator_thetaInternal_value                     : y =    2.93982e-05 yp =              0
generator_uStatorPu_im                            : y =      -0.147809 yp =              0
generator_uStatorPu_re                            : y =       0.969706 yp =              0
generator_udPu                                    : y =    2.88099e-05 yp =              0
generator_ufPu                                    : y =     0.00035194 yp =              0
generator_uqPu                                    : y =       0.979989 yp =              0
governor_PmPu_value                               : y =    1.18798e-08 yp =              0
governor_PmRawPu_u1                               : y =    2.49216e-09 yp =              0
governor_PmRawPu_u2                               : y =    9.38761e-09 yp =              0
governor_PmRawPu_y                                : y =    1.18798e-08 yp =              0
governor_PmRefPu_value                            : y =    2.49216e-09 yp =              0
governor_feedback_u1                              : y =              1 yp =              0
governor_feedback_u2                              : y =              1 yp =              0
governor_feedback_y                               : y =    1.87752e-09 yp =              0
governor_gain_u                                   : y =    1.87752e-09 yp =              0
governor_gain_y                                   : y =    9.38761e-09 yp =              0
governor_limiter_u                                : y =    1.18798e-08 yp =              0
governor_limiter_y                                : y =    1.18798e-08 yp =              0
governor_omegaPu_value                            : y =              1 yp =              0
governor_omegaRefPu_y                             : y =              1 yp =              0
voltageRegulator_EfdPu                            : y =       0.981736 yp =              0
voltageRegulator_EfdPuPin_value                   : y =       0.981736 yp =              0
voltageRegulator_UcEfdPu                          : y =        1.02999 yp =              0
voltageRegulator_UsPu                             : y =       0.980907 yp =              0
voltageRegulator_feedback_u1                      : y =        1.02999 yp =              0
voltageRegulator_feedback_u2                      : y =       0.980907 yp =              0
voltageRegulator_feedback_y                       : y =      0.0490868 yp =              0
voltageRegulator_gain_u                           : y =      0.0490868 yp =              0
voltageRegulator_gain_y                           : y =       0.981736 yp =              0
voltageRegulator_limiterWithLag_u                 : y =       0.981736 yp =              0
voltageRegulator_limiterWithLag_y                 : y =       0.981736 yp =              0
 ====== INIT DISCRETE VARIABLES VALUES ======
voltageRegulator_limiterWithLag_tUMaxReached      : z =          1e+60
voltageRegulator_limiterWithLag_tUMinReached      : z =          1e+60
generator_running_value                           : z =              1
generator_switchOffSignal1_value                  : z =             -1
generator_switchOffSignal2_value                  : z =             -1
generator_switchOffSignal3_value                  : z =             -1
voltageRegulator_limitationDown_value             : z =             -1
voltageRegulator_limitationUp_value               : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMax  : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMin  : z =             -1
generator_state                                   : z =              2
governor_state                                    : z =              1
 ====== PARAMETERS VALUES ======
Pm_Value0                                          =    2.49216e-09
URef_Value0                                        =        1.02999
generator_Ce0Pu                                    =    2.24294e-09
generator_Cm0Pu                                    =    2.49216e-09
generator_DPu                                      =              0
generator_Efd0Pu                                   =       0.982116
generator_H                                        =            5.4
generator_IRotor0Pu                                =       0.982116
generator_IStator0Pu                               =     0.00887755
generator_Id0Pu                                    =   -0.000792638
generator_If0Pu                                    =       0.417744
generator_Iq0Pu                                    =   -2.28379e-09
generator_Kuf                                      =    0.000358487
generator_LDPPu                                    =       0.142318
generator_LQ1PPu                                   =       0.261099
generator_LQ2PPu                                   =       0.125948
generator_LambdaD0Pu                               =       0.980253
generator_LambdaQ10Pu                              =   -5.36918e-09
generator_LambdaQ20Pu                              =   -5.36918e-09
generator_Lambdad0Pu                               =           0.98
generator_Lambdaf0Pu                               =        1.06561
generator_Lambdaq0Pu                               =   -6.09771e-09
generator_LdPPu                                    =          0.219
generator_LfPPu                                    =        0.20434
generator_LqPPu                                    =          0.219
generator_MdPPu                                    =          2.351
generator_MqPPu                                    =          2.351
generator_MrcPPu                                   =              0
generator_P0Pu                                     =             -0
generator_PGen0Pu                                  =              0
generator_PNom                                     =           1008
generator_Pm0Pu                                    =    2.49216e-09
generator_Q0Pu                                     =        -0.0087
generator_QGen0Pu                                  =         0.0087
generator_QStator0Pu                               =     0.00870788
generator_RDPPu                                    =      0.0181282
generator_RQ1PPu                                   =     0.00830709
generator_RQ2PPu                                   =      0.0191489
generator_RTfPu                                    =              0
generator_RTfoPu                                   =              0
generator_RaPPu                                    =        0.00357
generator_RfPPu                                    =    0.000842804
generator_SNom                                     =           1120
generator_SnTfo                                    =           1120
generator_Theta0                                   =      -0.151323
generator_ThetaInternal0                           =      -0.151323
generator_U0Pu                                     =           0.98
generator_UBaseHV                                  =             69
generator_UBaseLV                                  =             24
generator_UNom                                     =             24
generator_UNomHV                                   =             69
generator_UNomLV                                   =             24
generator_UPhase0                                  =       -0.15132
generator_UStator0Pu                               =       0.980888
generator_Ud0Pu                                    =   -2.82362e-06
generator_Uf0Pu                                    =    0.000352076
generator_Uq0Pu                                    =           0.98
generator_XTfPu                                    =            0.1
generator_XTfoPu                                   =            0.1
generator_i0Pu_im                                  =     0.00877611
generator_i0Pu_re                                  =     0.00133823
generator_iStator0Pu_im                            =     0.00877611
generator_iStator0Pu_re                            =     0.00133823
generator_rTfoPu                                   =              1
generator_s0Pu_im                                  =        -0.0087
generator_s0Pu_re                                  =             -0
generator_sStator0Pu_im                            =    -0.00870788
generator_sStator0Pu_re                            =     2.1684e-19
generator_u0Pu_im                                  =      -0.147728
generator_u0Pu_re                                  =       0.968801
generator_uStator0Pu_im                            =      -0.147862
generator_uStator0Pu_re                            =       0.969679
governor_KGover                                    =              5
governor_PMax                                      =           1008
governor_PMaxPu                                    =              1
governor_PMin                                      =              0
governor_PMinPu                                    =              0
governor_PNom                                      =           1008
governor_Pm0Pu                                     =    2.49216e-09
governor_PmRawPu_k1                                =              1
governor_PmRawPu_k2                                =              1
governor_gain_k                                    =              5
governor_limiter_uMax                              =              1
governor_limiter_uMin                              =              0
governor_omegaRefPu_k                              =              1
voltageRegulator_Efd0Pu                            =       0.982116
voltageRegulator_Efd0PuLF                          =       0.982116
voltageRegulator_EfdMaxPu                          =              5
voltageRegulator_EfdMinPu                          =             -5
voltageRegulator_Gain                              =             20
voltageRegulator_LagEfdMax                         =              0
voltageRegulator_LagEfdMin                         =              0
voltageRegulator_UcEfd0Pu                          =        1.02999
voltageRegulator_Us0Pu                             =       0.980888
voltageRegulator_gain_k                            =             20
voltageRegulator_limiterWithLag_LagMax             =              0
voltageRegulator_limiterWithLag_LagMin             =              0
voltageRegulator_limiterWithLag_UMax               =              5
voltageRegulator_limiterWithLag_UMin               =             -5
voltageRegulator_limiterWithLag_tUMaxReached0      =          1e+60
voltageRegulator_limiterWithLag_tUMinReached0      =          1e+60
voltageRegulator_limiterWithLag_u0                 =       0.982116
voltageRegulator_limiterWithLag_y0                 =       0.982116
voltageRegulator_tEfdMaxReached0                   =          1e+60
voltageRegulator_tEfdMinReached0                   =          1e+60
governor_limiter_limitsAtInit                      =              1
governor_limiter_strict                            =             -1
generator_ExcitationPu                             =              1
generator_NbSwitchOffSignals                       =              3
generator_State0                                   =              2
