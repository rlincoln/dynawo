 ====== INIT VARIABLES VALUES ======
generator_lambdaDPu                               : y =        1.00984 yp =   -3.23933e-05
generator_lambdaQ1Pu                              : y =   -1.49986e-09 yp =    9.47379e-06
generator_lambdaQ2Pu                              : y =   -1.49986e-09 yp =     5.0098e-05
generator_lambdafPu                               : y =        1.12075 yp =   -2.29605e-05
generator_omegaPu_value                           : y =              1 yp =    1.23067e-06
generator_omegaRefPu_value                        : y =              1 yp =              0
generator_terminal_V_im                           : y =     -0.0209623 yp =              0
generator_terminal_V_re                           : y =        1.00977 yp =              0
generator_theta                                   : y =      -0.020768 yp =    2.20778e-07
Pm_setPoint_value                                 : y =    7.11142e-10 yp =              0
URef_setPoint_value                               : y =        1.05969 yp =              0
generator_IRotorPu_value                          : y =        1.00871 yp =              0
generator_IStatorPu_value                         : y =      0.0071643 yp =              0
generator_PGen                                    : y =     -0.0457341 yp =              0
generator_PGenPu                                  : y =   -0.000457341 yp =              0
generator_PePu                                    : y =   -2.77171e-05 yp =              0
generator_PmPu_value                              : y =    -2.5157e-09 yp =              0
generator_QGen                                    : y =      -0.722142 yp =              0
generator_QGenPu                                  : y =    -0.00722142 yp =              0
generator_QStatorPu_value                         : y =    -0.00721629 yp =              0
generator_UPu                                     : y =        1.00999 yp =              0
generator_UStatorPu_value                         : y =        1.00928 yp =              0
generator_cePu                                    : y =   -2.77171e-05 yp =              0
generator_cmPu                                    : y =    -2.5157e-09 yp =              0
generator_efdPu_value                             : y =        1.00829 yp =              0
generator_iDPu                                    : y =    1.02247e-05 yp =              0
generator_iQ1Pu                                   : y =   -8.25572e-06 yp =              0
generator_iQ2Pu                                   : y =   -1.77806e-05 yp =              0
generator_iStatorPu_im                            : y =    -0.00715783 yp =              0
generator_iStatorPu_re                            : y =    0.000304322 yp =              0
generator_idPu                                    : y =    0.000433331 yp =              0
generator_ifPu                                    : y =       0.394951 yp =              0
generator_iqPu                                    : y =    2.74485e-05 yp =              0
generator_lambdadPu                               : y =        1.00999 yp =              0
generator_lambdaqPu                               : y =      1.311e-05 yp =              0
generator_sStatorPu_im                            : y =     0.00721629 yp =              0
generator_sStatorPu_re                            : y =    0.000457341 yp =              0
generator_terminal_i_im                           : y =    -0.00715783 yp =              0
generator_terminal_i_re                           : y =    0.000304322 yp =              0
generator_thetaInternal_value                     : y =   -1.16246e-05 yp =              0
generator_uStatorPu_im                            : y =     -0.0209927 yp =              0
generator_uStatorPu_re                            : y =        1.00906 yp =              0
generator_udPu                                    : y =   -1.17407e-05 yp =              0
generator_ufPu                                    : y =    0.000354783 yp =              0
generator_uqPu                                    : y =        1.00999 yp =              0
governor_PmPu_value                               : y =    -2.5157e-09 yp =              0
governor_PmRawPu_u1                               : y =    7.11142e-10 yp =              0
governor_PmRawPu_u2                               : y =   -3.22684e-09 yp =              0
governor_PmRawPu_y                                : y =    -2.5157e-09 yp =              0
governor_PmRefPu_value                            : y =    7.11142e-10 yp =              0
governor_feedback_u1                              : y =              1 yp =              0
governor_feedback_u2                              : y =              1 yp =              0
governor_feedback_y                               : y =   -6.45369e-10 yp =              0
governor_gain_u                                   : y =   -6.45369e-10 yp =              0
governor_gain_y                                   : y =   -3.22684e-09 yp =              0
governor_limiter_u                                : y =    -2.5157e-09 yp =              0
governor_limiter_y                                : y =    -2.5157e-09 yp =              0
governor_omegaPu_value                            : y =              1 yp =              0
governor_omegaRefPu_y                             : y =              1 yp =              0
voltageRegulator_EfdPu                            : y =        1.00829 yp =              0
voltageRegulator_EfdPuPin_value                   : y =        1.00829 yp =              0
voltageRegulator_UcEfdPu                          : y =        1.05969 yp =              0
voltageRegulator_UsPu                             : y =        1.00928 yp =              0
voltageRegulator_feedback_u1                      : y =        1.05969 yp =              0
voltageRegulator_feedback_u2                      : y =        1.00928 yp =              0
voltageRegulator_feedback_y                       : y =      0.0504145 yp =              0
voltageRegulator_gain_u                           : y =      0.0504145 yp =              0
voltageRegulator_gain_y                           : y =        1.00829 yp =              0
voltageRegulator_limiterWithLag_u                 : y =        1.00829 yp =              0
voltageRegulator_limiterWithLag_y                 : y =        1.00829 yp =              0
 ====== INIT DISCRETE VARIABLES VALUES ======
voltageRegulator_limiterWithLag_tUMaxReached      : z =          1e+60
voltageRegulator_limiterWithLag_tUMinReached      : z =          1e+60
generator_running_value                           : z =              1
generator_switchOffSignal1_value                  : z =             -1
generator_switchOffSignal2_value                  : z =             -1
generator_switchOffSignal3_value                  : z =             -1
voltageRegulator_limitationDown_value             : z =             -1
voltageRegulator_limitationUp_value               : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMax  : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMin  : z =             -1
generator_state                                   : z =              2
governor_state                                    : z =              2
 ====== PARAMETERS VALUES ======
Pm_Value0                                          =    7.11142e-10
URef_Value0                                        =        1.05969
generator_Ce0Pu                                    =    6.40028e-10
generator_Cm0Pu                                    =    7.11142e-10
generator_DPu                                      =              0
generator_Efd0Pu                                   =        1.00869
generator_H                                        =          5.625
generator_IRotor0Pu                                =        1.00869
generator_IStator0Pu                               =     0.00742574
generator_Id0Pu                                    =    0.000450045
generator_If0Pu                                    =       0.394945
generator_Iq0Pu                                    =    -6.3446e-10
generator_Kuf                                      =    0.000351866
generator_LDPPu                                    =       0.159961
generator_LQ1PPu                                   =       0.403952
generator_LQ2PPu                                   =       0.186362
generator_LambdaD0Pu                               =        1.00984
generator_LambdaQ10Pu                              =   -1.49986e-09
generator_LambdaQ20Pu                              =   -1.49986e-09
generator_Lambdad0Pu                               =           1.01
generator_Lambdaf0Pu                               =        1.12075
generator_Lambdaq0Pu                               =   -1.72573e-09
generator_LdPPu                                    =          0.256
generator_LfPPu                                    =       0.280818
generator_LqPPu                                    =          0.256
generator_MdPPu                                    =          2.554
generator_MqPPu                                    =          2.364
generator_MrcPPu                                   =              0
generator_P0Pu                                     =             -0
generator_PGen0Pu                                  =              0
generator_PNom                                     =           1485
generator_Pm0Pu                                    =    7.11142e-10
generator_Q0Pu                                     =         0.0075
generator_QGen0Pu                                  =        -0.0075
generator_QStator0Pu                               =    -0.00749449
generator_RDPPu                                    =       0.020223
generator_RQ1PPu                                   =     0.00728154
generator_RQ2PPu                                   =      0.0179934
generator_RTfPu                                    =              0
generator_RTfoPu                                   =              0
generator_RaPPu                                    =        0.00316
generator_RfPPu                                    =    0.000898666
generator_SNom                                     =           1650
generator_SnTfo                                    =           1650
generator_Theta0                                   =      -0.020768
generator_ThetaInternal0                           =      -0.020768
generator_U0Pu                                     =           1.01
generator_UBaseHV                                  =             69
generator_UBaseLV                                  =             20
generator_UNom                                     =             20
generator_UNomHV                                   =             69
generator_UNomLV                                   =             20
generator_UPhase0                                  =     -0.0207694
generator_UStator0Pu                               =        1.00926
generator_Ud0Pu                                    =    1.42387e-06
generator_Uf0Pu                                    =    0.000354924
generator_Uq0Pu                                    =           1.01
generator_XTfPu                                    =            0.1
generator_XTfoPu                                   =            0.1
generator_i0Pu_im                                  =    -0.00742414
generator_i0Pu_re                                  =   -0.000154217
generator_iStator0Pu_im                            =    -0.00742414
generator_iStator0Pu_re                            =   -0.000154217
generator_rTfoPu                                   =              1
generator_s0Pu_im                                  =         0.0075
generator_s0Pu_re                                  =             -0
generator_sStator0Pu_im                            =     0.00749449
generator_sStator0Pu_re                            =    5.42101e-20
generator_u0Pu_im                                  =     -0.0209756
generator_u0Pu_re                                  =        1.00978
generator_uStator0Pu_im                            =     -0.0209602
generator_uStator0Pu_re                            =        1.00904
governor_KGover                                    =              5
governor_PMax                                      =           1485
governor_PMaxPu                                    =              1
governor_PMin                                      =              0
governor_PMinPu                                    =              0
governor_PNom                                      =           1485
governor_Pm0Pu                                     =    7.11142e-10
governor_PmRawPu_k1                                =              1
governor_PmRawPu_k2                                =              1
governor_gain_k                                    =              5
governor_limiter_uMax                              =              1
governor_limiter_uMin                              =              0
governor_omegaRefPu_k                              =              1
voltageRegulator_Efd0Pu                            =        1.00869
voltageRegulator_Efd0PuLF                          =        1.00869
voltageRegulator_EfdMaxPu                          =              5
voltageRegulator_EfdMinPu                          =             -5
voltageRegulator_Gain                              =             20
voltageRegulator_LagEfdMax                         =              0
voltageRegulator_LagEfdMin                         =              0
voltageRegulator_UcEfd0Pu                          =        1.05969
voltageRegulator_Us0Pu                             =        1.00926
voltageRegulator_gain_k                            =             20
voltageRegulator_limiterWithLag_LagMax             =              0
voltageRegulator_limiterWithLag_LagMin             =              0
voltageRegulator_limiterWithLag_UMax               =              5
voltageRegulator_limiterWithLag_UMin               =             -5
voltageRegulator_limiterWithLag_tUMaxReached0      =          1e+60
voltageRegulator_limiterWithLag_tUMinReached0      =          1e+60
voltageRegulator_limiterWithLag_u0                 =        1.00869
voltageRegulator_limiterWithLag_y0                 =        1.00869
voltageRegulator_tEfdMaxReached0                   =          1e+60
voltageRegulator_tEfdMinReached0                   =          1e+60
governor_limiter_limitsAtInit                      =              1
governor_limiter_strict                            =             -1
generator_ExcitationPu                             =              1
generator_NbSwitchOffSignals                       =              3
generator_State0                                   =              2
