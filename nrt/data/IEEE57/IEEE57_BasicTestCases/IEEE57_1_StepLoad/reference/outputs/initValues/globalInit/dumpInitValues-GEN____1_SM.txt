 ====== INIT VARIABLES VALUES ======
generator_lambdaDPu                               : y =       0.962438 yp =   -2.19049e-05
generator_lambdaQ1Pu                              : y =      -0.471704 yp =   -4.49634e-06
generator_lambdaQ2Pu                              : y =      -0.471704 yp =   -2.38717e-05
generator_lambdafPu                               : y =        1.12697 yp =   -1.23852e-05
generator_omegaPu_value                           : y =              1 yp =   -8.53145e-07
generator_omegaRefPu_value                        : y =              1 yp =              0
generator_terminal_V_im                           : y =   -7.94586e-06 yp =              0
generator_terminal_V_re                           : y =           1.04 yp =              0
generator_theta                                   : y =       0.547272 yp =   -1.22524e-07
Pm_setPoint_value                                 : y =       0.311303 yp =              0
URef_setPoint_value                               : y =        1.32642 yp =              0
generator_IRotorPu_value                          : y =        1.49654 yp =              0
generator_IStatorPu_value                         : y =        4.76662 yp =              0
generator_PGen                                    : y =         478.69 yp =              0
generator_PGenPu                                  : y =         4.7869 yp =              0
generator_PePu                                    : y =        0.28019 yp =              0
generator_PmPu_value                              : y =       0.311303 yp =              0
generator_QGen                                    : y =        128.851 yp =              0
generator_QGenPu                                  : y =        1.28851 yp =              0
generator_QStatorPu_value                         : y =        3.56058 yp =              0
generator_UPu                                     : y =           1.04 yp =              0
generator_UStatorPu_value                         : y =         1.2516 yp =              0
generator_cePu                                    : y =        0.28019 yp =              0
generator_cmPu                                    : y =       0.311303 yp =              0
generator_efdPu_value                             : y =        1.49632 yp =              0
generator_iDPu                                    : y =    6.65291e-06 yp =              0
generator_iQ1Pu                                   : y =    3.80828e-06 yp =              0
generator_iQ2Pu                                   : y =    8.15691e-06 yp =              0
generator_iStatorPu_im                            : y =        1.23899 yp =              0
generator_iStatorPu_re                            : y =       -4.60278 yp =              0
generator_idPu                                    : y =      -0.201938 yp =              0
generator_ifPu                                    : y =       0.565801 yp =              0
generator_iqPu                                    : y =      -0.192153 yp =              0
generator_lambdadPu                               : y =       0.888729 yp =              0
generator_lambdaqPu                               : y =      -0.541842 yp =              0
generator_sStatorPu_im                            : y =       -3.56058 yp =              0
generator_sStatorPu_re                            : y =        -4.7869 yp =              0
generator_terminal_i_im                           : y =        1.23899 yp =              0
generator_terminal_i_re                           : y =       -4.60278 yp =              0
generator_thetaInternal_value                     : y =        0.54728 yp =              0
generator_uStatorPu_im                            : y =        0.46027 yp =              0
generator_uStatorPu_re                            : y =         1.1639 yp =              0
generator_udPu                                    : y =        0.54118 yp =              0
generator_ufPu                                    : y =    0.000526501 yp =              0
generator_uqPu                                    : y =         0.8881 yp =              0
governor_PmPu_value                               : y =       0.311303 yp =              0
governor_PmRawPu_u1                               : y =       0.311303 yp =              0
governor_PmRawPu_u2                               : y =    2.23696e-09 yp =              0
governor_PmRawPu_y                                : y =       0.311303 yp =              0
governor_PmRefPu_value                            : y =       0.311303 yp =              0
governor_feedback_u1                              : y =              1 yp =              0
governor_feedback_u2                              : y =              1 yp =              0
governor_feedback_y                               : y =    4.47393e-10 yp =              0
governor_gain_u                                   : y =    4.47393e-10 yp =              0
governor_gain_y                                   : y =    2.23696e-09 yp =              0
governor_limiter_u                                : y =       0.311303 yp =              0
governor_limiter_y                                : y =       0.311303 yp =              0
governor_omegaPu_value                            : y =              1 yp =              0
governor_omegaRefPu_y                             : y =              1 yp =              0
voltageRegulator_EfdPu                            : y =        1.49632 yp =              0
voltageRegulator_EfdPuPin_value                   : y =        1.49632 yp =              0
voltageRegulator_UcEfdPu                          : y =        1.32642 yp =              0
voltageRegulator_UsPu                             : y =         1.2516 yp =              0
voltageRegulator_feedback_u1                      : y =        1.32642 yp =              0
voltageRegulator_feedback_u2                      : y =         1.2516 yp =              0
voltageRegulator_feedback_y                       : y =       0.074816 yp =              0
voltageRegulator_gain_u                           : y =       0.074816 yp =              0
voltageRegulator_gain_y                           : y =        1.49632 yp =              0
voltageRegulator_limiterWithLag_u                 : y =        1.49632 yp =              0
voltageRegulator_limiterWithLag_y                 : y =        1.49632 yp =              0
 ====== INIT DISCRETE VARIABLES VALUES ======
voltageRegulator_limiterWithLag_tUMaxReached      : z =          1e+60
voltageRegulator_limiterWithLag_tUMinReached      : z =          1e+60
generator_running_value                           : z =              1
generator_switchOffSignal1_value                  : z =             -1
generator_switchOffSignal2_value                  : z =             -1
generator_switchOffSignal3_value                  : z =             -1
voltageRegulator_limitationDown_value             : z =             -1
voltageRegulator_limitationUp_value               : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMax  : z =             -1
voltageRegulator_limiterWithLag_initSaturatedMin  : z =             -1
generator_state                                   : z =              2
governor_state                                    : z =              1
 ====== PARAMETERS VALUES ======
Pm_Value0                                          =       0.311303
URef_Value0                                        =        1.32642
generator_Ce0Pu                                    =       0.280173
generator_Cm0Pu                                    =       0.311303
generator_DPu                                      =              0
generator_Efd0Pu                                   =        1.49653
generator_H                                        =          5.112
generator_IRotor0Pu                                =        1.49653
generator_IStator0Pu                               =        4.76634
generator_Id0Pu                                    =      -0.201927
generator_If0Pu                                    =       0.565797
generator_Iq0Pu                                    =       -0.19214
generator_Kuf                                      =    0.000351864
generator_LDPPu                                    =       0.167025
generator_LQ1PPu                                   =       0.419118
generator_LQ2PPu                                   =       0.194431
generator_LambdaD0Pu                               =       0.962438
generator_LambdaQ10Pu                              =      -0.471704
generator_LambdaQ20Pu                              =      -0.471704
generator_Lambdad0Pu                               =       0.888734
generator_Lambdaf0Pu                               =        1.12697
generator_Lambdaq0Pu                               =      -0.541835
generator_LdPPu                                    =          0.265
generator_LfPPu                                    =       0.290806
generator_LqPPu                                    =          0.265
generator_MdPPu                                    =          2.645
generator_MqPPu                                    =          2.455
generator_MrcPPu                                   =              0
generator_P0Pu                                     =        -4.7866
generator_PGen0Pu                                  =         4.7866
generator_PNom                                     =           1539
generator_Pm0Pu                                    =       0.311303
generator_Q0Pu                                     =        -1.2885
generator_QGen0Pu                                  =         1.2885
generator_QStator0Pu                               =         3.5603
generator_RDPPu                                    =      0.0210097
generator_RQ1PPu                                   =     0.00749885
generator_RQ2PPu                                   =      0.0187068
generator_RTfPu                                    =              0
generator_RTfoPu                                   =              0
generator_RaPPu                                    =       0.003275
generator_RfPPu                                    =     0.00093068
generator_SNom                                     =           1710
generator_SnTfo                                    =           1710
generator_Theta0                                   =       0.547272
generator_ThetaInternal0                           =       0.547272
generator_U0Pu                                     =           1.04
generator_UBaseHV                                  =             69
generator_UBaseLV                                  =             20
generator_UNom                                     =             20
generator_UNomHV                                   =             69
generator_UNomLV                                   =             20
generator_UPhase0                                  =              0
generator_UStator0Pu                               =        1.25159
generator_Ud0Pu                                    =       0.541174
generator_Uf0Pu                                    =    0.000526576
generator_Uq0Pu                                    =       0.888105
generator_XTfPu                                    =            0.1
generator_XTfoPu                                   =            0.1
generator_i0Pu_im                                  =        1.23894
generator_i0Pu_re                                  =        -4.6025
generator_iStator0Pu_im                            =        1.23894
generator_iStator0Pu_re                            =        -4.6025
generator_rTfoPu                                   =              1
generator_s0Pu_im                                  =        -1.2885
generator_s0Pu_re                                  =        -4.7866
generator_sStator0Pu_im                            =        -3.5603
generator_sStator0Pu_re                            =        -4.7866
generator_u0Pu_im                                  =              0
generator_u0Pu_re                                  =           1.04
generator_uStator0Pu_im                            =        0.46025
generator_uStator0Pu_re                            =        1.16389
governor_KGover                                    =              5
governor_PMax                                      =           1539
governor_PMaxPu                                    =              1
governor_PMin                                      =              0
governor_PMinPu                                    =              0
governor_PNom                                      =           1539
governor_Pm0Pu                                     =       0.311303
governor_PmRawPu_k1                                =              1
governor_PmRawPu_k2                                =              1
governor_gain_k                                    =              5
governor_limiter_uMax                              =              1
governor_limiter_uMin                              =              0
governor_omegaRefPu_k                              =              1
voltageRegulator_Efd0Pu                            =        1.49653
voltageRegulator_Efd0PuLF                          =        1.49653
voltageRegulator_EfdMaxPu                          =              5
voltageRegulator_EfdMinPu                          =             -5
voltageRegulator_Gain                              =             20
voltageRegulator_LagEfdMax                         =              0
voltageRegulator_LagEfdMin                         =              0
voltageRegulator_UcEfd0Pu                          =        1.32642
voltageRegulator_Us0Pu                             =        1.25159
voltageRegulator_gain_k                            =             20
voltageRegulator_limiterWithLag_LagMax             =              0
voltageRegulator_limiterWithLag_LagMin             =              0
voltageRegulator_limiterWithLag_UMax               =              5
voltageRegulator_limiterWithLag_UMin               =             -5
voltageRegulator_limiterWithLag_tUMaxReached0      =          1e+60
voltageRegulator_limiterWithLag_tUMinReached0      =          1e+60
voltageRegulator_limiterWithLag_u0                 =        1.49653
voltageRegulator_limiterWithLag_y0                 =        1.49653
voltageRegulator_tEfdMaxReached0                   =          1e+60
voltageRegulator_tEfdMinReached0                   =          1e+60
governor_limiter_limitsAtInit                      =              1
governor_limiter_strict                            =             -1
generator_ExcitationPu                             =              1
generator_NbSwitchOffSignals                       =              3
generator_State0                                   =              2
